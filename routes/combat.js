var express = require('express');
var router = express.Router();

// matrice pour les résultats des combats
var matrice = [[[0, 'K'], [0, 'K'], [0, 8], [0, 6], [1, 6], [2, 5], [3, 5], [4, 5], [5, 4], [6, 4], [7, 4], [8, 3], [9, 3]],
			[[0, 'K'], [0, 8], [0, 7], [1, 6], [2, 5], [3, 5], [4, 4], [5, 4], [6, 3], [7, 3], [8, 3], [9, 3], [10, 2]],
			[[0, 8], [0, 7], [1, 6], [2, 5], [3, 5], [4, 4], [5, 4], [6, 3], [7, 3], [8, 3], [9, 2], [10, 2], [11, 2]],
			[[0, 8], [1, 7], [2, 6], [3, 5], [4, 4], [5, 4], [6, 3], [7, 3], [8, 2], [9, 2], [10, 2], [11, 2], [12, 2]],
			[[1, 7], [2, 6], [3, 5], [4, 4], [5, 4], [6, 3], [7, 2], [8, 2], [9, 2], [10, 2], [11, 2], [12, 2], [14, 1]],
			[[2, 6], [3, 6], [4, 5], [5, 4], [6, 3], [7, 2], [8, 2], [9, 2], [10, 2], [11, 1], [12, 1], [14, 1], [16, 1]],
			[[3, 5], [4, 5], [5, 4], [6, 3], [7, 2], [8, 2], [9, 1], [10, 1], [11, 1], [12, 0], [14, 0], [16, 0], [18, 0]],
			[[4, 4], [5, 4], [6, 3], [7, 2], [8, 1], [9, 1], [10, 0], [11, 0], [12, 0], [14, 0], [16, 0], [18, 0], ['K', 0]],
			[[5, 3], [6, 3], [7, 2], [8, 0], [9, 0], [10, 0], [11, 0], [12, 0], [14, 0], [16, 0], [18, 0], ['K', 0], ['K', 0]],
			[[6, 0], [7, 0], [8, 0], [9, 0], [10,0], [11, 0], [12, 0], [14, 0], [16, 0], [18, 0], ['K', 0], ['K', 0], ['K', 0]]];

// service web pour le combat stateless
router.get('/:habJoueur/:habEnnemi', function(req, res, next) {
  // On récupère le paramètre de l'URL
  var habJoueur = req.params.habJoueur;
  var habEnnemi = req.params.habEnnemi;
  var quotient = habJoueur - habEnnemi;

  // transformation du quotient pour le tableau
  var col = 0;
  if (quotient >= 11)
  	col = 12;
  else if (quotient == 10 || quotient == 9)
  	col = 11;
  else if (quotient == 8 || quotient == 7)
  	col = 10;
  else if (quotient == 6 || quotient == 5)
  	col = 9;
  else if (quotient == 4 || quotient == 3)
  	col = 8;
  else if (quotient == 2 || quotient == 1)
  	col = 7;
  else if (quotient == 0)
  	col = 6;
  else if (quotient == -2 || quotient == -1)
  	col = 5;
  else if (quotient == -4 || quotient == -3)
  	col = 4;
  else if (quotient == -6 || quotient == -5)
  	col = 3;
  else if (quotient == -8 || quotient == -7)
  	col = 2;
  else if (quotient == -10 || quotient == -9)
  	col = 1;
  else 
  	col = 0;

  // calcul valeur aléatoire
  var nbAleatoire = Math.round(Math.random()*9);
  var rang = nbAleatoire - 1;
  if (rang < 0)
  	rang = 9;

  var endurancePerduEnnemi = matrice[rang][col][0];
  var endurancePerduJoueur = matrice[rang][col][1];

  // création du json
  combat = {}
  combat["quotient"] = quotient;
  combat["nbAleatoire"] = nbAleatoire;
  combat["endurancePerduEnnemi"] = endurancePerduEnnemi;
  combat["endurancePerduJoueur"] = endurancePerduJoueur;

  res.json(combat);
});

module.exports = router;