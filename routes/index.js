var express = require('express');
var router = express.Router();
var utilities = require('../scripts/jsUtilities');
var session = require('express-session');
var dec = require('../scripts/decision');
var disciplinesJSON = require('../scripts/disciplines.json');
var objetsJSON = require('../scripts/objets.json');
var database = require('../scripts/database');
router.use(session({
  secret:'secresstttt',
  resave: false,
  saveUninitialized: true,
  cookie: {secure: true}
}));

router.post('/saveChar', function(req,res,next) {
  var disciplines = utilities.extractDisciplines(req.body);
  var objets = utilities.extractObjects(req.body);
  var endurance = parseInt(req.body["endurance"]);
  var habilete = parseInt(req.body["habilete"]);
  var or = req.body["or"];

  if (utilities.validateCreationForm(disciplines["actives"], objets) && req.body["name"] != "")
  {
    // sauvegarde des données dans Objet joueur
    // création des attirbuts de base
    var Joueur = {};
    Joueur["name"] = req.body["name"];
    Joueur["abilityBase"] = habilete;
    Joueur["enduranceBase"] = endurance; 
    Joueur["enduranceMax"] = endurance;
    Joueur["or"] = or;
    Joueur["disciplines"] = disciplines["actives"];
    Joueur["objets"] = objets;
    Joueur["abilityBonus"] = 0;
    Joueur["enduranceBonus"] = 0;
    Joueur["combatHistory"] = [];
    // calcul des attributs avec modification

    if(objets["objetSpeciaux"].indexOf(objetsJSON.objetSpeciaux.GILETCUIR) >= 0)
    {  
      Joueur["enduranceBonus"] += 2;
    }
    if(objets.armes.length > 0)
    {
      if(objets["armes"].indexOf(disciplines["masteredWeapon"]) >= 0)
      {
        Joueur["abilityBonus"] +=4 
      }
      else
      {
        Joueur["abilityBonus"] +=2
      }
    }
    // sauvegarder joueur dans session
    session.joueur = Joueur;
    session.joueurId = utilities.generateUUID();

    var JoueurProgression = {};
    JoueurProgression["page"] = 1;
    JoueurProgression["section"] = 1;
    JoueurProgression["message"] = 1;
    JoueurProgression["vivant"] = 1;
    JoueurProgression["des"] = -1;

    database.addPlayer({"_id": session.joueurId, "playerData":Joueur}, function(result){
      database.addPlayerProgression(session.joueurId, JoueurProgression, function(result){
        // redirect vers page 1
        res.redirect('/page');
      });
    });
  }
  else
  {
    // si les valeurs sont mauvaises, redirecte vers création
    res.render('creation', {title: "Création du joueur", habilete: habilete , endurance: endurance, or: or, nbMaitriseMax:5, nbObjectsMax:2, error:"Vous devez nommez votre personnage et choisir précisément 5 disciplines et 2 objets!" });
  }

});

router.post('/updatePlayer', function(req,res,next) {
  database.modifyPlayer(req.body["_id"], req.body["playerData"], function(result){
    res.json(result);
    });
});

router.post('/updatePlayerProgress', function(req,res,next) {
  database.modifyPlayerProgression(req.body["_id"], req.body["playerProg"], function(result){
    res.json(result);
    });
});

router.get('/', function(req, res) {
    // Cette fonction va tenter de convertir le fichier jade situé à
    // view/index.jade en HTML pour l'envoyer vers le client.
    // On profite pour envoyer au client l'attribut « title ».
    //res.render('index', { title: "Index" });
    res.render('accueil', { title: "Accueil" });
});

router.get('/accueil', function(req, res) {
    // Cette fonction va tenter de convertir le fichier jade situé à
    // view/index.jade en HTML pour l'envoyer vers le client.
    // On profite pour envoyer au client l'attribut « title ».
    res.render('accueil', { title: "Accueil" });
});

router.get('/creation', function(req, res) {
  res.render('creation', {title: "Création du joueur", habilete: Math.round(Math.random()*(9) + 10) , endurance: Math.round(Math.random()*(9) + 20), or: Math.round(Math.random()*(9) + 10), nbMaitriseMax:5, nbObjectsMax:2});
});

router.get('/jeu', function(req, res) {
    res.render('jeu', { title: "Page de Jeu" });
});

router.get('/combat', function(req, res) {
    res.render('combat', { title: "Combat", ennemi:{endurance:30 , habilete:19 , image:"./images/imagePage78.png"}});
});

router.get('/allPlayers', function(req, res) {
  database.getAllPlayers(function(result){res.json(result)});
});

router.get('/player/:id', function(req, res) {
  var playerId = req.params.id
  database.getPlayer(playerId,function(result){res.json(result)});
});

router.get('/currentPlayer', function(req, res) {
  database.getPlayer(session.joueurId,function(result){res.json(result)});
});

router.get('/currentProgression', function(req, res) {
  database.getPlayerProgression(session.joueurId,function(result){res.json(result)});
});

router.get('/playerProgression/:id', function(req, res) {
  var playerId = req.params.id
  database.getPlayerProgression(playerId,function(result){res.json(result)});
});

router.get('/deletePlayer/:id', function(req, res, next) {
  var playerId = req.params.id
  database.deletePlayer(playerId, function(result){
    res.redirect("/creation");
    });
});

router.get('/deleteProgression/:id', function(req, res, next) {
  var playerId = req.params.id
  database.deletePlayerProgression(id, function(result){
    database.addPlayerProgression(id, {"page" : 1, "section" : 1}, function(result){
    });
  });
});

router.get('/resume/:id', function(req, res) {
  var playerId = req.params.id;
  database.getPlayer(playerId,function(resultPlayer){
    session.joueur = resultPlayer.playerData;
    session.joueurId = playerId;

    res.redirect("/page");

    //database.getPlayerProgression(playerId,function(resultProg){
      //res.redirect("/page/"+resultProg.playerProgression.page)});
  });
});

router.get('/aide', function(req, res) {
    // Cette fonction va tenter de convertir le fichier jade situé à
    // view/index.jade en HTML pour l'envoyer vers le client.
    // On profite pour envoyer au client l'attribut « title ».
    res.render('aide', { title: "aide" });
});

router.get('/dead/:playerId', function(req, res) {
  var playerId = req.params.playerId;
  database.deletePlayer(playerId, function(result){
    database.deletePlayerProgression(playerId, function(result){
      res.json(result);
    });
  });
});

router.get('/jeu/:page', function(req, res, next) {
  // On récupère le paramètre de l'URL
  var v = req.params.page

  var files = utilities.getAllFilesFromFolder( __dirname + '/../views/page' + v);
  result = {};
  result["pageId"] = v;
  result["html"] = {};

  for (var i = 0; i < files.length; i++) {
    var name = "./page"+v+"/"+files[i];
    res.render(name, function(err, html) {
      result["html"][i+1] = html;
    });  

  var pageRouteIndex = "page_"+v;
  result["routes"] = dec[pageRouteIndex];
  }

  // mise à jour de la BD pour la progression du joueur
  // réinitialisation de la progression dans une page
 // var JoueurProgression = {};
  //JoueurProgression["page"] = v;
  //JoueurProgression["section"] = 1;
  //JoueurProgression["popup"] = 1;
  //database.modifyPlayerProgression(session.joueurId, JoueurProgression, function(result){
   // });


  res.json(result);
});

router.get('/jeu/:page/:section', function(req, res, next) {
  var p = req.params.page;
  var s = req.params.section;
  
  var page = "./page" + p + "/" + s + ".jade"
  
  res.render(page, function(err, html) {
        res.render('page', { title: 'Page ' + p, htmlPage: html})
  });
});

router.get('/page', function(req, res, next) {
  res.render('page');
});

router.get('/page/:page', function(req, res, next) {
  var v = req.params.page
  var redirectExt = "/page/" + v + "/1";

  res.redirect(redirectExt);
});


router.get('/page/:page/:section', function(req, res, next) {
  // On récupère le paramètre de l'URL
  var p = req.params.page;
  var s = req.params.section;
  // On crée dynamiquement la page qu'on souhaite charger
  var files = utilities.getAllFilesFromFolder( __dirname + '/../views/page' + p);
  var htmlString = "";
  for (var i = 0; i < Math.min(files.length, s); i++) {
    var name = "./page"+p+"/"+files[i];
    res.render(name, function(err, html) {
      htmlString += html;
    });  
  }

  var page = "./page" + p + "/" + s + ".jade"
  // On veut d'abord convertir la page en HTML, une fois que la conversion
  // est faite, on va injecter le HTML généré vers le fichier page.jade

  database.modifyPlayerProgression(session.joueurId, {"page" : parseInt(p), "section" : s}, function(result){
    res.render(page, function(err, html) {
        res.render('page', { title: 'Page ' + p, htmlPage: htmlString})
    });
  });
});

module.exports = router;