var app = angular.module('log4420App', ['ngSanitize']);


app.controller('ControllerPage',  function($scope, $rootScope, $sce, $http, htmlFactory){

	$scope.adventure = {};
	$scope.decision = {};
	$rootScope.player = {};	
	$rootScope.playerProg = {};
	
	$http.get('http://localhost:3000/currentPlayer')
		.then(function(response){
			console.log(response.data);
			$rootScope.player = response.data.playerData;
			$rootScope.playerId = response.data._id;		
		});

	$http.get('http://localhost:3000/currentProgression')
	.then(function(responsePlayer){
		console.log(responsePlayer.data);
		$rootScope.playerProg = responsePlayer.data.playerProgression;
		$scope.page = $rootScope.playerProg.page;

		htmlFactory.getPage($scope.page).then(
			function(response){
				$scope.adventure = $sce.trustAsHtml(response.html['1']);
				$scope.decision = $sce.trustAsHtml(response.html['2']);
			});
	});

	
	$scope.loadPage =  function(page){
		htmlFactory.getPage(page).then(
			function(response){
				$rootScope.playerProg.page = page;
				$rootScope.playerProg.section = 1;
				$rootScope.playerProg.message = 1;
				$rootScope.playerProg.des = -1;
				$http.post("http://localhost:3000/updatePlayerProgress", {"_id": $rootScope.playerId, "playerProg":$rootScope.playerProg})
					.then(function(a){

						// Condition de la guérison qui ne doit avoir lieu qu'une seule fois après un chargement
						if($scope.hasDiscipline('Guérison'))
						{
							$rootScope.playerProg.message = 2;
							$rootScope.player.enduranceBase += 1;

							if ($rootScope.player.enduranceBase > $rootScope.player.enduranceMax)
								$rootScope.player.enduranceBase = $rootScope.player.enduranceMax;
							else
							{
								// mettre à jour le serveur
								$http.post("http://localhost:3000/updatePlayer", {"_id": $rootScope.playerId, "playerData" : $rootScope.player})
									.then(function(b){});
							}	
						}
					});

				$scope.page = page;
				$scope.adventure = $sce.trustAsHtml(response.html['1']);
				$scope.decision = $sce.trustAsHtml(response.html['2']);

				
			});
	};

	$scope.hasDiscipline = function(discipline){
		var discArray = $rootScope.player.disciplines;
		var result = discArray.indexOf(discipline);

		if (result == -1)
			return false;
		else
			return true;
	}

	$scope.hasObject = function(obj){
		var objArray = $rootScope.player.objets.sacADos;
		var result = objArray.indexOf(obj);

		if (result == -1)
			return false;
		else
			return true;
	}

	$scope.hasSpecialObject = function(specialObject){
		var objArray = $rootScope.player.objets.objetSpeciaux;
		var result = objArray.indexOf(specialObject);

		if (result == -1)
			return false;
		else
			return true;
	}

	$scope.detruireJoueur = function(){
		$http.get('http://localhost:3000/dead/' + $rootScope.playerId)
				.then(function(response){});
		$rootScope.playerProg.alive = 0;
	}

});

app.directive('compileTemplate', function($compile, $parse){
    return {
        link: function(scope, element, attr){
            var parsed = $parse(attr.ngBindHtml);
            function getStringValue() { return (parsed(scope) || '').toString(); }

            //Recompile if the template changes
            scope.$watch(getStringValue, function() {
                $compile(element, null, -9999)(scope);  //The -9999 makes it skip directives so that we do not recompile ourselves
            });
        }         
    }
});

app.factory('htmlFactory', function($http){
	var htmlFactory = {
		getPage: function(page){
			var promise = $http.get('http://localhost:3000/jeu/' + page )
				.then(function(response){
					return response.data;
				},
				function(error){
					return error;
				});
			return promise;
		}
	};

	return htmlFactory;
});

app.controller('sidebarController', function($scope, $rootScope) {

	$scope.calculateHability = function(){
		if($rootScope.player.abilityBase !== undefined)
			return $rootScope.player.abilityBase.toString() + " + " + $rootScope.player.abilityBonus.toString() + " = " + ($rootScope.player.abilityBase+$rootScope.player.abilityBonus).toString();
	};

	$scope.calculateEndurance = function(){
		if($rootScope.player.enduranceBase !== undefined)
			return $rootScope.player.enduranceBase.toString() + " + " + $rootScope.player.enduranceBonus.toString() + " = " + ($rootScope.player.enduranceBase+$rootScope.player.enduranceBonus).toString();
	};
});

app.controller('choixController', function($scope, $rootScope, $http) {

	$scope.checkbox12 = {
		repas3 : false,
		fur : false,
		corde : false,
	};

	$scope.checkbox57 = {
		bonesword : false,
		disk : false,
	};

	$scope.accepteChoix = function(){
		if ($scope.checkbox12.fur)
			$rootScope.player.objets.sacADos.push("fourrure")
		if ($scope.checkbox12.corde)
			$rootScope.player.objets.sacADos.push("corde")
		if ($scope.checkbox12.repas3)
		{
			$rootScope.player.objets.sacADos.push("rations spéciales")
			$rootScope.player.objets.sacADos.push("rations spéciales")
			$rootScope.player.objets.sacADos.push("rations spéciales")
		}
		if ($scope.checkbox57.bonesword)
			$rootScope.player.objets.objetSpeciaux.push("épée d'os")
		if ($scope.checkbox57.disk)
			$rootScope.player.objets.objetSpeciaux.push("disque")

		$rootScope.playerProg.section += 1;

		$http.post("http://localhost:3000/updatePlayer", {"_id": $rootScope.playerId, "playerData" : $rootScope.player})
			.then(function(response){});
		$http.post("http://localhost:3000/updatePlayerProgress", {"_id": $rootScope.playerId, "playerProg":$rootScope.playerProg})
			.then(function(response){});
	}

	$scope.retirerObjet = function(obj){
		var objArray = $rootScope.player.objets.sacADos;
		var index = objArray.indexOf(obj);

		$rootScope.playerProg.section += 1;

		if(index != -1)
		{
			objArray.splice(index, 1);

			$http.post("http://localhost:3000/updatePlayer", {"_id": $rootScope.playerId, "playerData" : $rootScope.player})
				.then(function(response){});
			$http.post("http://localhost:3000/updatePlayerProgress", {"_id": $rootScope.playerId, "playerProg":$rootScope.playerProg})
				.then(function(response){});
			return true;
		}
		else
		{
			$http.post("http://localhost:3000/updatePlayer", {"_id": $rootScope.playerId, "playerData" : $rootScope.player})
				.then(function(response){});
			$http.post("http://localhost:3000/updatePlayerProgress", {"_id": $rootScope.playerId, "playerProg":$rootScope.playerProg})
				.then(function(response){});
			return false;
		}
	}

	$scope.prendreDmg = function(dmg){

		$rootScope.player.enduranceBase -= dmg;

		if($rootScope.player.enduranceBase + $rootScope.player.enduranceBonus <= 0)
		{
			$http.get('http://localhost:3000/dead/' + $rootScope.playerId)
				.then(function(response){});
			$rootScope.playerProg.alive = 0;
		}
		else
		{
			$rootScope.playerProg.section += 1;

			$http.post("http://localhost:3000/updatePlayer", {"_id": $rootScope.playerId, "playerData" : $rootScope.player})
				.then(function(response){});
			$http.post("http://localhost:3000/updatePlayerProgress", {"_id": $rootScope.playerId, "playerProg":$rootScope.playerProg})
				.then(function(response){});
		}
	}

	$scope.ajouterObjetSpecial = function(obj){
		$rootScope.player.objets.objetSpeciaux.push(obj);

		$rootScope.playerProg.section += 1;

		$http.post("http://localhost:3000/updatePlayer", {"_id": $rootScope.playerId, "playerData" : $rootScope.player})
			.then(function(response){});
		$http.post("http://localhost:3000/updatePlayerProgress", {"_id": $rootScope.playerId, "playerProg":$rootScope.playerProg})
			.then(function(response){});

	}
	
});

app.controller('combatController', function($scope, $rootScope, $http) {
	$rootScope.combatStats = [];
	$scope.fightStart = function(name, ability, endurance, autoDamage, page){
		if($rootScope.playerProg.page == page && $rootScope.playerProg.combatProgress !== undefined && $rootScope.playerProg.combatProgress.ennemiName == name)
		{
			var combatProgress = $rootScope.playerProg.combatProgress;
			$rootScope.combatStats = combatProgress.combatStats;
			$scope.ennemiAbility = combatProgress.ennemiAbility;
			$scope.ennemiEndurance = combatProgress.ennemiEndurance;
		}
		else
		{
			$scope.ennemiAbility = ability;
			$scope.ennemiEndurance = endurance;
			$rootScope.playerProg.page = page;
		}
		$scope.ennemiName = name;
		$scope.autoDamage = autoDamage;
		$scope.combatOver = false;
		$scope.flawless = true;

		if($rootScope.player.disciplines !== undefined 
			&& $rootScope.player.disciplines.indexOf("Puissance pshychique") == -1)
			document.getElementById("psychicPower").style.visibility = "hidden";

	};

	$scope.fight = function(isPsychique){
		if(!$scope.combatOver)
		{
			$http.get($scope.buildFightQuery(isPsychique))
			.then(function(response){
				var turnStat = {};
				turnStat.tour = $rootScope.combatStats.length+1;
				turnStat.ratio = response.data.quotient;
				turnStat.des = response.data.nbAleatoire;
				
				if(response.data.endurancePerduEnnemi == "K")
					turnStat.dmgDone = $scope.ennemiEndurance
				else
					turnStat.dmgDone = response.data.endurancePerduEnnemi;

				if(response.data.endurancePerduJoueur == "K")
					turnStat.dmgReceived = $rootScope.player.enduranceBase + $rootScope.player.enduranceBonus
				else
					turnStat.dmgReceived = response.data.endurancePerduJoueur;

				if($scope.flawless && turnStat.dmgReceived > 0)
					$scope.flawless = false;

				$scope.ennemiEndurance -= turnStat.dmgDone + $scope.autoDamage;
				$rootScope.player.enduranceBase -= turnStat.dmgReceived;
				
				if($scope.autoDamage > 0)
					turnStat.dmgDone = turnStat.dmgDone.toString() + " + " + $scope.autoDamage.toString();
				$rootScope.combatStats.push(turnStat);

				if(($rootScope.player.enduranceBase+$rootScope.player.enduranceBonus) <= 0)
					$scope.fightEnd(false, false);
				else if($scope.ennemiEndurance <= 0)
					$scope.fightEnd(true, false);

				$scope.updateServerValues();
			});
		}
	};

	$scope.run = function(){
		if(!$scope.combatOver)
		{
			$http.get($scope.buildFightQuery(false))
			.then(function(response){
				var turnStat = {};
				turnStat.tour = $rootScope.combatStats.length+1;
				turnStat.ratio = "fuite";
				turnStat.des = "---";
				turnStat.dmgDone = "---";
				
				if(response.data.endurancePerduJoueur == "K")
					turnStat.dmgReceived = $rootScope.player.enduranceBase + $rootScope.player.enduranceBonus
				else
					turnStat.dmgReceived = response.data.endurancePerduJoueur;
				
				$rootScope.combatStats.push(turnStat);
				$rootScope.player.enduranceBase -= turnStat.dmgReceived;
				$scope.updateServerValues();

				if(($rootScope.player.enduranceBase+$rootScope.player.enduranceBonus) <= 0)
					$scope.fightEnd(false, false);
				else
					$scope.fightEnd(false, true);
			});	
		}	
	};

	$scope.buildFightQuery = function(isPsychique){
		var ability = $rootScope.player.abilityBase+$rootScope.player.abilityBonus;
		if(isPsychique) ability +=2;

		return 'http://localhost:3000/combat/'+ ability + '/' + $scope.ennemiAbility;
	};

	$scope.fightEnd = function(won, ran){
		$scope.combatOver = true;
		document.getElementById("combatButtonRow").style.display = "none";
		var combatEntry = {"ennemi" : $scope.ennemiName, "tour": $rootScope.combatStats.length};
		if(won && !ran)
		{	
			if($scope.flawless && document.getElementById("flawlessWin") !== undefined)
				document.getElementById("flawlessWin").style.visibility = "visible";
			else if(document.getElementById("win") !== undefined)
				document.getElementById("win").style.visibility = "visible";
			combatEntry["result"] = "victoire";
		}
		else if (ran)
		{
			if(document.getElementById("run") !== undefined)
				document.getElementById("run").style.visibility = "visible";
			combatEntry["result"] = "fuite";
		}
		else
		{
			$http.get("http://localhost:3000/dead/"+$rootScope.playerId).then(function(response){});
			$rootScope.playerProg.alive = 0;
			combatEntry["result"] = "mort";
		}
		$rootScope.player.combatHistory.push(combatEntry);
	};

	$scope.updateServerValues = function(){
		$rootScope.playerProg.combatProgress = {"combatStats":$scope.combatStats, "ennemiEndurance" : $scope.ennemiEndurance, "ennemiAbility" : $scope.ennemiAbility, "ennemiName":$scope.ennemiName}
		$http.post("http://localhost:3000/updatePlayer", {"_id": $rootScope.playerId, "playerData" : $rootScope.player})
			.then(function(response){
				$http.post("http://localhost:3000/updatePlayerProgress", {"_id": $rootScope.playerId, "playerProg":$rootScope.playerProg})
				.then(function(response){});
			});
		
	}
});

app.controller('objectsController', function($scope, $rootScope) {
	$scope.bag = [];
	$scope.specialObjects = [];
	$rootScope.$watch('player', function(newVal, oldVal) {
		if($rootScope.player.objets !== undefined)
		{
			$scope.bag = [];
			$scope.specialObjects = [];
			for(var i = 0; i< $rootScope.player.objets.sacADos.length; i++ )
			{
					var object = {};
					object["name"] = $rootScope.player.objets.sacADos[i];
					object["path"] =  $scope.imageSourceForObject($rootScope.player.objets.sacADos[i]);
					$scope.bag.push(object);
			}
			for(var i = 0; i< $rootScope.player.objets.objetSpeciaux.length; i++ )
			{
					var object = {};
					object["name"] = $rootScope.player.objets.objetSpeciaux[i];
					object["path"] =  $scope.imageSourceForObject($rootScope.player.objets.objetSpeciaux[i]);
					$scope.specialObjects.push(object);			
			}
		}	
	});

	$scope.imageSourceForObject = function(object){
		var imPath = "../images/"
		switch(object){
			case "potion de lampsur" :
				return imPath + "potionLampsur.png";
			case "rations spéciales" :
				return imPath + "rations.png";
			case "corde" :
				return imPath + "rope.png";
			case "fourrure" :
				return imPath + "fur.png";			
			case "disque" :
				return imPath + "stonedisk.png";
			case "Gilet de cuir matelassé" :
				return imPath + "paddedLeather.png";	
			case "Huile de Bakanal":
				return imPath + "oil.png";	
			case "épée d'os":
				return imPath + "bonesword.png";	
		}
		return "";
	};
});

app.controller('aleatoireController', function($scope, $rootScope, $http) {

	$scope.HasardBonus = function(){

		var bonus = 0;
		if ($rootScope.player.enduranceBase > 20)
			bonus = 1;
		else if($rootScope.player.enduranceBase < 10)
			bonus = -2;

		$http.get('http://localhost:3000/aleatoire/' + $rootScope.playerProg.page + '/' + bonus)
			.then(function(response){
				console.log(response.data);
				$rootScope.playerProg.des = response.data.destination;
				$rootScope.playerProg.section += 1;

				$http.post("http://localhost:3000/updatePlayerProgress", {"_id": $rootScope.playerId, "playerProg":$rootScope.playerProg})
					.then(function(response){});
			});

	}

	$scope.Hasard = function(){

		$http.get('http://localhost:3000/aleatoire/' + $rootScope.playerProg.page + '/' + 0)
			.then(function(response){
				console.log(response.data);
				$rootScope.playerProg.des = response.data.destination;

				$rootScope.playerProg.section += 1;
				$http.post("http://localhost:3000/updatePlayerProgress", {"_id": $rootScope.playerId, "playerProg":$rootScope.playerProg})
					.then(function(response){});

			});

		
	}
	
});