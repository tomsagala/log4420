var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
var url = 'mongodb://dbUser:qwerty@ds045464.mongolab.com:45464/log4420';
var mongoDB;

MongoClient.connect(url, function(err,db){
	console.log("connecting to db ... ")
	if(err)
	{
		console.log('MongoDB connection failure');
	}
	else
	{
		console.log("db connection successful")
		mongoDB = db;
	}
})

module.exports = {
	addPlayer: function(player, callback)
	{
		mongoDB.collection('players').insertOne(player, function(err,result)
			{
				assert.equal(err, null);
				console.log("One insertion to players collection");
				callback(result);
			});
	},

	getAllPlayers: function(callback) 
	{
		mongoDB.collection('players').find().toArray(function(err,result)
			{
				assert.equal(err, null);
				callback(result);
			});
	},

	getPlayer: function(playerId, callback)
	{
		mongoDB.collection('players').find({"_id": playerId}).toArray(function(err,result)
			{
				assert.equal(err, null);
				callback(result[0]);
			});
	},

	modifyPlayer: function(playerId, player, callback) 
	{
		mongoDB.collection('players').updateOne({"_id" : playerId}, {"playerData" : player}, function(err,result)
			{
				assert.equal(err, null);
				console.log("Modified player " + playerId);
				callback(result);
			});
	},

	//delete the player and its progression
	deletePlayer: function(playerId, callback) 
	{
		mongoDB.collection('players').deleteOne({"_id" : playerId}, function(err,result)
			{
				assert.equal(err, null);
				console.log("Player " + playerId + " has been deleted (RIP)");
				var resultTotal = [];
				resultTotal.push(result);
				mongoDB.collection('playerState').deleteOne({"_id" : playerId}, function(err,result)
				{
					assert.equal(err, null);
					console.log("Progression has been deleted  for player " + playerId);
					resultTotal.push(result);
					callback(resultTotal);
				});
			});

		
	},

	addPlayerProgression: function(playerId, playerProgression, callback)
	{
		mongoDB.collection('playerState').insertOne({"_id":playerId, "playerProgression": playerProgression}, function(err,result)
			{
				assert.equal(err, null);
				console.log("One insertion to players collection");
				callback(result);
			});
	},

	modifyPlayerProgression: function(playerId, playerProgression, callback) 
	{
		mongoDB.collection('playerState').updateOne({"_id" : playerId}, {"playerProgression":playerProgression}, function(err,result)
			{
				assert.equal(err, null);
				console.log("Modified progression for player " + playerId);
				callback(result);
			});
	},

	deletePlayerProgression: function(playerId, callback) 
	{
		mongoDB.collection('playerState').deleteOne({"_id" : playerId}, function(err,result)
			{
				assert.equal(err, null);
				console.log("Progression has been deleted  for player " + playerId);
				callback(result);
			});
	},

	getPlayerProgression: function(playerId, callback)
	{
		mongoDB.collection('playerState').find({"_id": playerId}).toArray(function(err,result)
			{
				assert.equal(err, null);
				callback(result[0]);
			});
	}
};