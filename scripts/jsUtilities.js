var disciplinesJSON = require('./disciplines.json');
var objetsJSON = require('./objets.json');

module.exports = {
	getAllFilesFromFolder: function(dir) {
		var filesystem = require("fs");
	  	var results = [];

		filesystem.readdirSync(dir).forEach(function(file) 
		{
	    	fullFileName = dir+'/'+file;
		    var stat = filesystem.statSync(fullFileName);

		    if (stat && stat.isDirectory()) 
		    {
		      results = results.concat(getAllFilesFromFolder(fullFileName))
		    }
		    else results.push(file);
	  	});
	  	return results;
	},

	validateCreationForm: function(disciplines, objets)
	{
		return disciplines.length == 5 && objets["armes"].length + objets["sacADos"].length + objets["objetSpeciaux"].length == 2;
	},

	extractDisciplines: function(postData)
	{
		var disciplines = {};
		disciplines["actives"] = [];
		disciplines["masteredWeapon"] = "";

		Object.keys(postData).forEach(function(key, index)
		{
			if(disciplinesJSON[key] != null)
			{
				if(key != "MAITRISEDESARMES")
				{
					disciplines["actives"].push(disciplinesJSON[key]);
				}
			}
			else if(disciplinesJSON["MAITRISEDESARMES"][key] != null)
			{
				disciplines["actives"].push(disciplinesJSON["MAITRISEDESARMES"][key]["name"]);
				disciplines["masteredWeapon"] = objetsJSON["armes"][disciplinesJSON["MAITRISEDESARMES"][key]["weaponKey"]];
			}
		})
		return disciplines;
	},
	
	extractObjects: function(postData)
	{
		var objets = {};
		objets["armes"] = [];
		objets["sacADos"] = [];
		objets["objetSpeciaux"] = [];
		Object.keys(postData).forEach(function(key, index)
		{
			if(objetsJSON["armes"][key] != null )
			{
				objets["armes"].push(objetsJSON["armes"][key]);
			}
			else if( objetsJSON["sacADos"][key] != null )
			{
				objets["sacADos"].push(objetsJSON["sacADos"][key]);
			}
			else if( objetsJSON["objetSpeciaux"][key] != null )
			{
				objets["objetSpeciaux"].push(objetsJSON["objetSpeciaux"][key]);
			}
		})
		return objets;
	},

	randomNumberForElement: function(min, max)
	{
		return Math.round(Math.random()*(max-min) + min);
	},

	generateUUID: function(){
    var d = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (d + Math.random()*16)%16 | 0;
        d = Math.floor(d/16);
        return (c=='x' ? r : (r&0x3|0x8)).toString(16);
    });
    return uuid;
	}
};