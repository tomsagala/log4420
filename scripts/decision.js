// structure pour choix
DECISION = {
  'page_1' : [
    {page: 160},
    {page: 273}
  ],
  'page_12' : [
    {page: 180},
    {page: 259}
  ],
  'page_129' : [
    {page: 155}
  ],
  'page_134' : [
    {page: 57, plage: [0,1,2,3]},
    {page: 188, plage: [4,5,6]},
    {page: 331, plage: [7,8,9]}
  ],
  'page_155' : [
    {page: 248, plage: [-2,-1,0,1,2]},
    {page: 191, plage: [3,4,5,6,7,8,9,10]}
  ],
  'page_160' : [
    {page: 78},
    {page: 204, discipline: 'CHASSE'},
    {page: 318, discipline: 'COMMUNICATIONANIMALE'}
  ],
  'page_167' : [
    {page: 85, plage: [0,1,2,3,4,5,6]},
    {page: 300, plage: [7,8,9]}
  ],
  'page_172' : [
    {page: 134}
  ],
  'page_180' : [
    {page: 70, condition: 'NONBLESSE'},
    {page: 129, condition: 'BLESSE'}
  ],
  'page_188' : [
    {page: 331}
  ],
  'page_204' : [
    {page: 134}
  ],
  'page_209' : [
    {page: 155}
  ],
  'page_245' : [
    {page: 91, objetSpecial: 'HUILE BAKANAL'},
    {page: 172}
  ],
  'page_248' : [
  ],
  'page_288' : [
    {page: 167}
  ],
  'page_300' : [
    {page: 12},
    {page: 238}
  ],
  'page_318' : [
    {page: 134},
    {page: 259}
  ],
  'page_331' : [
    {page: 62, plage: [0,1,2,3,4]},
    {page: 288, plage: [5,6,7,8,9]}
  ],
  'page_339' : [
  ],
  'page_57' : [
    {page: 331}
  ],
  'page_62' : [
    {page: 288}
  ],
  'page_70' : [
    {page: 209, objetSpecial: 'HUILE BAKANAL'},
    {page: 339}
  ],
  'page_78' : [
    {page: 245, condition: 'VICTOIRE'}
  ],
  'page_91' : [
    {page: 134}
  ]
};

module.exports = DECISION;